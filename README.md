# Budget Buddy

### Setup

**Using Docker**

```
sudo docker-compose up
```

**Manual**

```
git clone git@gitlab.com:bonitoflakesss/budgetbuddy.git

# Frontend
cd frontend && yarn

yarn && yarn dev

# Backend
cd backend && yarn

yarn && yarn start
```
