import pg from "pg";
import dotenv from "dotenv";
dotenv.config();

const connectionString = process.env.POSTGRES_CONNECTION_STRING;
const pool = new pg.Pool({
  connectionString,
  // ssl: {
  //   rejectUnauthorized: false, // disable SSL
  // },
});

const dbConnection = () => {
  pool.query("SELECT NOW()", (err, res) => {
    if (err) {
      console.error("Error executing query", err);
    } else {
      console.log(
        "Connected to PostgreSQL. Current timestamp:",
        res.rows[0].now
      );
    }
    pool.end();
  });
};

// dbConnection();

export default dbConnection;
