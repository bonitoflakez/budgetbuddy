import express from "express";
import cors from "cors";
import dbConnection from "./config/connection.js";

const app = express();

app.use(express.json());
app.use(cors());

const startServer = async () => {
  try {
    await dbConnection();

    app.get("/health", (req, res) => {
      return res.status(200).json({
        status: "Server is live",
        message: "Server is live",
      });
    });

    app.get("/hello", (req, res, next) => {
      return res.status(200).json({
        message: "Hi Budget Buddy!",
      });
    });

    app.listen(3000, () => {
      console.log("Server is up and running!");
    });
  } catch (error) {
    console.error("Error starting the server:", error.message);
    process.exit(1);
  }
};

startServer();
