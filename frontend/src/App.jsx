import { useEffect, useState } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const API_URL = "https://budget-buddy-nh7u.onrender.com";

function App() {
  const [message, setMessage] = useState("No message?");
  const [serverStatus, setServerStatus] = useState("loading");

  useEffect(() => {
    let isMounted = true;

    const checkServerHealth = async () => {
      try {
        const healthResponse = await axios.get(`${API_URL}/health`);
        if (isMounted) {
          if (healthResponse.status === 200) {
            setServerStatus("live");
            toast.success("Server is live");

            try {
              const messageResponse = await axios.get(`${API_URL}/hello`);
              setMessage(messageResponse.data.message);
            } catch (error) {
              console.error("Error fetching /hello data:", error);
            }
          } else {
            setServerStatus("error");
            toast.error("Unable to connect to the server.");
            toast.info("Retrying to connect to the server in 5 seconds...");

            // Reload page after 5 seconds
            setTimeout(() => {
              window.location.reload();
            }, 5000);
          }
        }
      } catch (error) {
        if (isMounted) {
          console.error("Error checking server health:", error);
          setServerStatus("error");
          toast.error("Unable to connect to the server.");
          toast.info("Retrying to connect to the server in 5 seconds...");

          // Reload page after 5 seconds
          setTimeout(() => {
            window.location.reload();
          }, 5000);
        }
      }
    };

    checkServerHealth();

    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <>
      <ToastContainer />
      {serverStatus === "loading" && <p>Waiting for the server to start...</p>}
      {serverStatus === "live" && (
        <>
          <h2 className="text-2xl font-bold text-center">{message}</h2>
        </>
      )}
      {serverStatus === "error" && <p>Server is not serving...</p>}
    </>
  );
}

export default App;
